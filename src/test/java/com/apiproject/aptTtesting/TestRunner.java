package com.apiproject.aptTtesting;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;



@CucumberOptions(features = "resources/feature", glue = "com.APIProjectStepDefinition", dryRun = true, monochrome = true, plugin = {
		"pretty", "html:target/htmlreports","json:target/cucumber.json","html:target/site/cucumber-pretty" })

public class TestRunner extends AbstractTestNGCucumberTests {

}